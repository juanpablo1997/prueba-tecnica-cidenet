package WebService;

import Modelo.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import Modelo.Usuario;
import javax.swing.JOptionPane;

/**
 *  
 * @author Juan Pablo
 */
@WebService(serviceName = "PruebaTecnicaWS")
public class PruebaTecnicaWS extends ConexionBD {
    ArrayList<Usuario> array = new ArrayList<Usuario>();

    /**
     * Web service operation
     * @param nombre_usuario
     * @param tipo_documento
     * @param numero_documento
     * @return 
     */
    @WebMethod(operationName = "RegistroUsuario")
    public int RegistroUsuario(@WebParam(name = "nombre_usuario") String nombre_usuario, @WebParam(name = "tipo_documento") String tipo_documento, @WebParam(name = "numero_documento") int numero_documento) {

    /*
    Se intenta crear y establecer una conexión con una base de datos en MySQL workbench sin exito
    luego de hacer algunas consultas y leer algunos documentos.
    Por ello se procede en un intento por terminar la prueba tecnica en adoptar una estructura de datos,
    en este caso un arraylist. Despues de intentarlo por un arraylist tampoco se pudo guardar por la aparicion 
    de un error.
    
    PreparedStatement ps = null;
    Connection conexion = getConexion();
    
    String sql = "INSERT INTO Registros_Usuarios (numero_documento, tipo_documento, nombre_usuario) VALUES (?,?,?)";
        try {
            ps = conexion.prepareStatement(sql);
            ps.setInt(1, numero_documento);
            ps.setString(2, tipo_documento);
            ps.setString(3, nombre_usuario);
            ps.execute();
            return 0;
            } catch (SQLException ex) {
            Logger.getLogger(PruebaTecnicaWS.class.getName()).log(Level.SEVERE, null, ex);
    
            return 1;
        }*/
        return 0;
    }
}
