package Modelo;

/**
 *
 * @author Juan Pablo
 */
public class Usuario {
    int numero_documento;
    String tipo_documento;
    String nombre_usuario;
    
    public Usuario() {
        this.numero_documento = 0;
        this.tipo_documento = "";
        this.nombre_usuario = "";
    }
    
    public Usuario(int numero_documento, String tipo_documento, String nombre_usuario) {
        this.numero_documento = numero_documento;
        this.tipo_documento = tipo_documento;
        this.nombre_usuario = nombre_usuario;
    }

    public int getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(int numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }
    
}
