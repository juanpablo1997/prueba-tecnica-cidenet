package clientepruebatecnicavista;

import ws.PruebaTecnicaWS_Service;
import ws.PruebaTecnicaWS;
import javax.swing.JOptionPane;
 
/**
 *
 * @author Juan Pablo
 */
public class RegistroUsuario extends javax.swing.JFrame {

   PruebaTecnicaWS_Service ptws_servicio = new PruebaTecnicaWS_Service();
   PruebaTecnicaWS ptws_cliente;
            
    public RegistroUsuario() {
        // Aspectos generales
        initComponents();
        this.setLocationRelativeTo(null);
        
        ptws_cliente = ptws_servicio.getPruebaTecnicaWSPort();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        nombre_usuario = new javax.swing.JTextField();
        tipo_documento = new javax.swing.JComboBox<>();
        numero_documento = new javax.swing.JTextField();
        Registrar_usuario = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        Informacion_letras = new javax.swing.JLabel();
        Cancelar_operacion = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registro de usuario");

        jLabel1.setText("Nombre de usuario");

        jLabel2.setText("Tipo de documento");

        jLabel3.setText("Número de documento");

        nombre_usuario.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        nombre_usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombre_usuarioActionPerformed(evt);
            }
        });
        nombre_usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nombre_usuarioKeyTyped(evt);
            }
        });

        tipo_documento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CC", "TI" }));
        tipo_documento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tipo_documento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipo_documentoActionPerformed(evt);
            }
        });

        numero_documento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numero_documentoActionPerformed(evt);
            }
        });
        numero_documento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numero_documentoKeyTyped(evt);
            }
        });

        Registrar_usuario.setText("Registrar usuario");
        Registrar_usuario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Registrar_usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Registrar_usuarioActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("REGISTRO DE USUARIO");

        Informacion_letras.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        Informacion_letras.setText("Debes ingresar solo números");

        Cancelar_operacion.setText("Cancelar");
        Cancelar_operacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Cancelar_operacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cancelar_operacionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nombre_usuario)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tipo_documento, 0, 141, Short.MAX_VALUE)
                            .addComponent(Cancelar_operacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 26, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(numero_documento, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addComponent(Registrar_usuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(Informacion_letras)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGap(1, 1, 1)))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(30, 30, 30)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nombre_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tipo_documento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(numero_documento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(Informacion_letras)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Registrar_usuario)
                    .addComponent(Cancelar_operacion))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nombre_usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombre_usuarioActionPerformed

    }//GEN-LAST:event_nombre_usuarioActionPerformed

    private void tipo_documentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tipo_documentoActionPerformed
        
    }//GEN-LAST:event_tipo_documentoActionPerformed

    private void numero_documentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numero_documentoActionPerformed
        
    }//GEN-LAST:event_numero_documentoActionPerformed

    private void Registrar_usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Registrar_usuarioActionPerformed
        /*Se evaluan los campos y se mandan los datos a la web service*/
        String nombre = nombre_usuario.getText();
        String tipo = tipo_documento.getSelectedItem().toString();
        String numero = numero_documento.getText();
        int verificacion = 0;
        
        // Esta condición valida si los campos están vacios.
        if(nombre_usuario.getText().isEmpty() || tipo_documento.getSelectedItem().toString().isEmpty() || numero_documento.getText().isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "Debes llenar todos los campos");
        } else {
            // Esta condición valida si los campos superan el límite de caracteres. 
            if(nombre_usuario.getText().length() > 100) {
                 
                verificacion = 1;
            } else if (numero_documento.getText().length() > 10) { 
                
                verificacion = 2;
            }
            
            switch(verificacion) {
                case 1: {
                    JOptionPane.showMessageDialog(rootPane, "El campo Nombre de usuario superó el líimite de 100 caracteres", "CÓDIGO DE RESPUESTA - 001", JOptionPane.INFORMATION_MESSAGE);
                    break;
                }
                case 2: {
                    JOptionPane.showMessageDialog(rootPane, "El campo Número de documentos superó el límite de 10 dígitos", "CÓDIGO DE RESPUESTA - 002", JOptionPane.INFORMATION_MESSAGE);
                    break;
                }
                case 0: {
                    int numeroint = Integer.parseInt(numero);
                    int respuesta = ptws_cliente.registroUsuario(nombre, tipo,numeroint);
                    JOptionPane.showMessageDialog(rootPane, "Se envio la información correctamente");
                    
                    break; 
                }
            }
            
        }
       
    }//GEN-LAST:event_Registrar_usuarioActionPerformed

    private void nombre_usuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nombre_usuarioKeyTyped
        // Validación de solo recibir números
        char validar = evt.getKeyChar();
        // Valida si los caracteres son letras y no numeros
        if(Character.isDigit(validar)){ 
            getToolkit().beep();
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar al campo solo valores de tipo caracter" );
        }
    }//GEN-LAST:event_nombre_usuarioKeyTyped

    private void numero_documentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numero_documentoKeyTyped
        // Validación de solo recibir números
        char validar = evt.getKeyChar();
        // Valida si los caracteres son numeros y no letras
        if(Character.isLetter(validar)){
            getToolkit().beep();
            evt.consume();
            
            JOptionPane.showMessageDialog(rootPane, "Debes ingresar al campo solo valores numericos" );
        }
    }//GEN-LAST:event_numero_documentoKeyTyped

    private void Cancelar_operacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Cancelar_operacionActionPerformed
       ProgramaPrincipal ventana = new ProgramaPrincipal();
       ventana.setVisible(true);
       this.setVisible(false);
    }//GEN-LAST:event_Cancelar_operacionActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new RegistroUsuario().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton Cancelar_operacion;
    private javax.swing.JLabel Informacion_letras;
    private javax.swing.JButton Registrar_usuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField nombre_usuario;
    private javax.swing.JTextField numero_documento;
    private javax.swing.JComboBox<String> tipo_documento;
    // End of variables declaration//GEN-END:variables

   
}
