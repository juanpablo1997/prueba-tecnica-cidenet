
package ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RegistroUsuarioResponse_QNAME = new QName("http://WebService/", "RegistroUsuarioResponse");
    private final static QName _RegistroUsuario_QNAME = new QName("http://WebService/", "RegistroUsuario");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RegistroUsuario }
     * 
     */
    public RegistroUsuario createRegistroUsuario() {
        return new RegistroUsuario();
    }

    /**
     * Create an instance of {@link RegistroUsuarioResponse }
     * 
     */
    public RegistroUsuarioResponse createRegistroUsuarioResponse() {
        return new RegistroUsuarioResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistroUsuarioResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "RegistroUsuarioResponse")
    public JAXBElement<RegistroUsuarioResponse> createRegistroUsuarioResponse(RegistroUsuarioResponse value) {
        return new JAXBElement<RegistroUsuarioResponse>(_RegistroUsuarioResponse_QNAME, RegistroUsuarioResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistroUsuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WebService/", name = "RegistroUsuario")
    public JAXBElement<RegistroUsuario> createRegistroUsuario(RegistroUsuario value) {
        return new JAXBElement<RegistroUsuario>(_RegistroUsuario_QNAME, RegistroUsuario.class, null, value);
    }

}
